RELEASE=4.2

PACKAGE=drbd-utils
PKGREL=1

DRBDVER=8.9.8
DRBDDIR=${PACKAGE}
DRBDSRC=${DRBDDIR}.tgz

DEBS= 						\
drbd8-utils_${DRBDVER}-${PKGREL}_amd64.deb  	\
drbd-utils_${DRBDVER}-${PKGREL}_amd64.deb

all: ${DEBS}
	echo ${DEBS}

.PHONY: deb
deb ${DEBS}: ${DRBDSRC}
	rm -rf ${DRBDDIR}
	tar xzf ${DRBDSRC}
	mv ${DRBDDIR}/debian ${DRBDDIR}/debian.old
	rsync -a debian ${DRBDDIR}
	cd ${DRBDDIR}; dpkg-buildpackage -rfakeroot -b -us -uc
	-lintian ${DEBS}

.PHONY: download
download ${DRBDSRC}:
	rm -rf ${DRBDDIR} ${DRBDSRC}
	git clone --recursive -b v${DRBDVER} git://git.drbd.org/drbd-utils
	cd ${DRBDDIR}; ./autogen.sh
	tar czf ${DRBDSRC} ${DRBDDIR}

.PHONY: upload
upload: ${DEBS}
	tar cf - ${DEBS} | ssh repoman@repo.proxmox.com upload

distclean: clean

clean:
	rm -rf ${DRBDDIR} *.deb *.changes
	find . -name '*~' -exec rm {} ';'

.PHONY: dinstall
dinstall: ${DEBS}
	dpkg -i ${DEBS}
